﻿using CollageAdministrator.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Data
{
    public class CollageAdministratorDbContext : DbContext
    {
        public CollageAdministratorDbContext(DbContextOptions<CollageAdministratorDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Grade>()
                .HasKey(g => new { g.AccountId, g.SubjectId });

            modelBuilder.Entity<RoleAccount>()
                .HasKey(ra => new { ra.RoleId, ra.AccountId });;

            modelBuilder.Entity<FacultySubject>()
                .HasKey(fs => new { fs.FacultyId, fs.SubjectId });

            modelBuilder.Entity<CourseSubject>()
                .HasKey(cs => new { cs.CourseId, cs.SubjectId });

            modelBuilder.Entity<CourseStudent>()
                .HasKey(cs => new { cs.CourseId, cs.AccountId });

            modelBuilder.Entity<StudentClazz>()
                .HasKey(sc => new { sc.AccountId, sc.ClazzId });
        }

        public DbSet<Account> Account { get; set; }
        public DbSet<Clazz> Clazz { get; set; }
        public DbSet<Subject> Subject { get; set; }
        public DbSet<StudentClazz> StudentClazz { get; set; }
        public DbSet<Grade> Grade { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<RoleAccount> RoleAccount { get; set; }
        public DbSet<GeneralInformation> GeneralInformation { get; set; }
        public DbSet<Credential> Credential { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<CourseSubject> CourseSubject { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Faculty> Faculty { get; set; }
        public DbSet<FacultySubject> FacultySubject { get; set; }
        public DbSet<CourseStudent> CourseStudents { get; set; }
    }
}
