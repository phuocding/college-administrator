﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CollageAdministrator.Data;
using CollageAdministrator.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CollageAdministrator.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CollageAdminAPIController : ControllerBase
    {
        private readonly CollageAdministratorDbContext _context;
        private readonly IHostingEnvironment env;

        public CollageAdminAPIController(CollageAdministratorDbContext context, IHostingEnvironment env)
        {
            _context = context;
            this.env = env;

        }

        public IActionResult Login([FromBody]Account account)
        {
            var existAccount = _context.Account
                .Where(a => a.Status != AccountStatus.Deactive)
                .Include(a => a.GeneralInformation)
                .SingleOrDefault(a => a.RollNumber == account.RollNumber);
            if (existAccount != null)
            {
                var roleAccounts = _context.RoleAccount
                    .Include(ra => ra.Role)
                    .Where(ra => ra.AccountId == existAccount.AccountId);
                account.Salt = existAccount.Salt;
                account.EncryptPassword(account.Password);
                var roles = "";
                if (existAccount.Password == account.Password && existAccount.ApprovalStatus == ApprovalStatus.Accepted)
                {
                    foreach (var ra in roleAccounts)
                    {
                        roles += ra.Role.Name + "#";
                    }
                    var id = existAccount.AccountId;
                    var credential = _context.Credential.SingleOrDefault(cr => cr.OwnerId == existAccount.AccountId);
                    if (credential == null)
                    {
                        credential = new Credential(existAccount.AccountId);
                        _context.Credential.Add(credential);
                        _context.SaveChanges();
                    }
                    Response.StatusCode = 200;
                    var result = new { credential, roles, existAccount.GeneralInformation.Name, id };
                    return new JsonResult(result);
                }
                else
                {
                    return Unauthorized();
                }
            }
            return NoContent();
        }

        public IActionResult ClazzsIndex([FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                var clazzs = _context.Clazz.ToList();
                return new JsonResult(clazzs);
            }
            return Unauthorized();
        }

        public IActionResult CreateClazz([FromBody]Clazz clazz, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var existClazz = _context.Clazz.Any(c => c.Name == clazz.Name);
                if(existClazz == false)
                {
                    if(clazz.FacultyId != 0)
                    {
                        var faculty = _context.Faculty.Find(clazz.FacultyId);
                        clazz.Faculty = faculty;
                        clazz.InFacultyStatus = InFaculty.Yes;
                    }
                    if (clazz.CourseId != 0)
                    {
                        var course = _context.Course.Find(clazz.CourseId);
                        clazz.Course = course;
                        clazz.InCourseStatus = InCourse.Yes;
                    }
                    _context.Add(clazz);
                    _context.SaveChanges();
                    return Ok();
                }
                return Conflict();      
            }
            return Unauthorized();
        }

        public IActionResult EditClazz([FromBody] Clazz clazz, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var existedClazz = _context.Clazz.Find(clazz.ClazzId);
                if (existedClazz != null)
                {
                    existedClazz.Name = clazz.Name;
                    existedClazz.Teacher = clazz.Teacher;
                    existedClazz.Status = clazz.Status;
                    existedClazz.UpdatedAt = DateTime.Today;
                    _context.Update(existedClazz);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult ClazzDetails([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int clazzId)
        {
            if (CheckToken(Authorization) == true)
            {
                var studentClazz = _context.StudentClazz
                    .Where(sc => sc.ClazzId == clazzId)
                    .Include(sc => sc.Account)
                    .ThenInclude(a => a.GeneralInformation)
                    .Include(sc => sc.Account)
                    .ThenInclude(a => a.RoleAccounts)
                    .ThenInclude(ra => ra.Role)
                    .ToList();
                var clazz = _context.Clazz
                    .Where(c => c.ClazzId == clazzId)
                    .Include(c => c.Faculty)
                    .ThenInclude(f => f.FacultySubjects)
                    .ThenInclude(fs => fs.Subject)
                    .Include(c => c.Course)
                    .ThenInclude(co => co.CourseSubjects)
                    .ThenInclude(fs => fs.Subject)
                    .ToList();
                var result = new { studentClazz, clazz };
                return new JsonResult(result);
            }
            return Unauthorized();
        }

        public IActionResult AccountsIndex([FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                var studentAccounts = _context.RoleAccount
                .Where(ra => ra.RoleId == 1)
                .Include(ra => ra.Account)
                .ThenInclude(a => a.GeneralInformation)
                .Include(ra => ra.Role)
                .ToList();
                var studentClazzs = _context.StudentClazz
                    .Include(sc => sc.Account)
                    .Include(sc => sc.Clazz)
                .ToList();
                var clazzs = _context.Clazz
                    .Where(c => c.Status == ClazzStatus.Active)
                    .Include(c => c.StudentClazzs)
                    .ToList();
                var result = new { studentClazzs, studentAccounts, clazzs };
                return new JsonResult(result);
            }
            return Unauthorized();
        }

        [HttpGet]
        public IActionResult CreateAccount([FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var roles = _context.Role
                    .Where(r => r.Status != RoleStatus.Deactive)
                    .ToList();
                return new JsonResult(roles);
            }
            return Unauthorized();
        }

        public IActionResult CreateAccount([FromBody] GeneralInformationWithRoles generalInfoWithRoles, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                if (ModelState.IsValid)
                {
                    Account account = new Account();
                    account.GeneralInformation = generalInfoWithRoles.GeneralInformation;
                    foreach (var id in generalInfoWithRoles.RoleIds)
                    {
                        var role = _context.Role.Find(id);
                        RoleAccount roleAccount = new RoleAccount
                        {
                            Role = role,
                            Account = account
                        };
                        _context.Add(roleAccount);
                    }
                    _context.Add(account);
                    _context.Add(account.GeneralInformation);
                    _context.SaveChanges();
                    account.RollNumber = "B19APTECH" + account.AccountId.ToString("D4");
                    account.Password = account.GeneralInformation.Dob.ToString("ddMMyy");
                    account.EncryptPassword(account.Password);
                    _context.Update(account);
                    _context.SaveChanges();
                    var builder = new StringBuilder();

                    using (var reader = System.IO.File.OpenText(env.WebRootPath + "/Templates/Email Templates/ApprovalStatusEmail.html"))
                    {
                        builder.Append(reader.ReadToEnd());
                    }
                    builder.Replace("{2}", account.GeneralInformation.Name);
                    builder.Replace("{7}", "Your application is being reviewed!");
                    builder.Replace("{6}", "Thank you for choosing our university. Please wait a few days for us to verify your application. You will receive an email whether your application is accepted or declined. You can also use your temporary rollnumber below to check your approval status.");
                    builder.Replace("{8}", "none");
                    SendEmail("Welcome", builder.ToString(), account.GeneralInformation.Email, account.GeneralInformation.Name);
                    return new JsonResult(account.GeneralInformation.Dob.ToString("ddMMyy"));
                }
            }
            return Unauthorized();
        }

        public IActionResult AddStudents([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int[] studentIds)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var clazzId = studentIds.Last();
                var clazz = _context.Clazz.Find(clazzId);
                for (var i = 0; i < studentIds.Count() - 1; i++)
                {
                    var existedStudentClazz = _context.StudentClazz.Find(studentIds[i], clazzId);
                    if (existedStudentClazz != null)
                    {
                        continue;
                    }
                    var student = _context.Account.Find(studentIds[i]);
                    StudentClazz studentClazz = new StudentClazz
                    {
                        Account = student,
                        Clazz = clazz
                    };
                    _context.Add(studentClazz);
                }
                _context.SaveChanges();
                return new JsonResult(studentIds);
            }
            return Unauthorized();
        }

        public IActionResult EditAccount([FromBody] GeneralInformationWithRoles generalInfoWithRoles, [FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                var account = _context.Account.Find(generalInfoWithRoles.AccountId);
                var generalInfo = _context.GeneralInformation.Find(generalInfoWithRoles.AccountId);
                account.RollNumber = "B19APTECH" + account.AccountId.ToString("D4");
                generalInfo.Phone = generalInfoWithRoles.GeneralInformation.Phone;
                generalInfo.PermanentAddress = generalInfoWithRoles.GeneralInformation.PermanentAddress;
                generalInfo.ResidentialAddress = generalInfoWithRoles.GeneralInformation.ResidentialAddress;
                generalInfo.Email = generalInfoWithRoles.GeneralInformation.Email;
                account.UpdatedAt = DateTime.Today;
                if (generalInfoWithRoles.Password != null)
                {
                    account.EncryptPassword(generalInfoWithRoles.Password);
                }
                if (generalInfoWithRoles.RoleIds != null)
                {
                    var OldRoleAccount = _context.RoleAccount.Where(ora => ora.AccountId == generalInfoWithRoles.AccountId);
                    _context.RoleAccount.RemoveRange(OldRoleAccount);
                }
                foreach (var roleId in generalInfoWithRoles.RoleIds)
                {
                    var role = _context.Role.Find(roleId);
                    RoleAccount roleAccount = new RoleAccount
                    {
                        Role = role,
                        Account = account
                    };
                    _context.RoleAccount.Add(roleAccount);
                }
                _context.Account.Update(account);
                _context.GeneralInformation.Update(generalInfo);
                _context.SaveChanges();
                return new JsonResult(generalInfoWithRoles);
            }
            return Unauthorized();
        }

        public IActionResult DeleteAccount([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int accountId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var account = _context.Account.Find(accountId);
                var roleAccounts = _context.RoleAccount.Where(ra => ra.AccountId == accountId);
                var studentClazzs = _context.StudentClazz.Where(sc => sc.AccountId == accountId);
                var grades = _context.Grade.Where(sc => sc.AccountId == accountId);
                if (account != null)
                {
                    if (roleAccounts != null)
                    {
                        foreach (var roleAccount in roleAccounts)
                        {
                            _context.RoleAccount.Remove(roleAccount);
                        }
                    }
                    if (studentClazzs != null)
                    {
                        foreach (var studentClazz in studentClazzs)
                        {
                            _context.StudentClazz.Remove(studentClazz);
                        }
                    }
                    if (grades != null)
                    {
                        foreach (var grade in grades)
                        {
                            _context.Grade.Remove(grade);
                        }
                    }
                    _context.Account.Remove(account);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult StudentDetails([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int studentId)
        {
            if (CheckToken(Authorization) == true)
            {
                var student = _context.Account
                     .Where(a => a.AccountId == studentId)
                     .Include(a => a.GeneralInformation)
                     .Include(a => a.RoleAccounts)
                     .ToList();
                return new JsonResult(student);
            }
            return Unauthorized();
        }

        public IActionResult AddGrades([FromBody]IEnumerable<Grade> grades, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                foreach (var grade in grades)
                {
                    var existedGrade = _context.Grade.Find(grade.AccountId, grade.SubjectId);
                    if (existedGrade != null)
                    {
                        return Conflict();
                    }
                    grade.AccountId = grade.AccountId;
                    grade.SubjectId = grade.SubjectId;
                    if (grade.TheoreticalGrade < 5)
                    {
                        grade.TheoreticalStatus = GradeStatus.Failed;
                    }
                    if (grade.AssignmentGrade < 5)
                    {
                        grade.AssignmentStatus = GradeStatus.Failed;
                    }
                    if (grade.PracticalGrade < 5)
                    {
                        grade.PracticalStatus = GradeStatus.Failed;
                    }
                    _context.Add(grade);
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult EditGrades([FromHeader] string Authorization, [FromHeader] string Role, [FromBody]IEnumerable<Grade> grades)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                foreach (var grade in grades)
                {
                    var existedGrade = _context.Grade.Find(grade.AccountId, grade.SubjectId);
                    if (existedGrade == null)
                    {
                        return NoContent();
                    }
                    existedGrade.AccountId = grade.AccountId;
                    existedGrade.SubjectId = grade.SubjectId;
                    existedGrade.AssignmentGrade = grade.AssignmentGrade;
                    existedGrade.PracticalGrade = grade.PracticalGrade;
                    existedGrade.TheoreticalGrade = grade.TheoreticalGrade;
                    existedGrade.UpdatedAt = DateTime.Today;
                    if (existedGrade.TheoreticalGrade < 5)
                    {
                        existedGrade.TheoreticalStatus = GradeStatus.Failed;
                    }
                    if (existedGrade.AssignmentGrade < 5)
                    {
                        existedGrade.AssignmentStatus = GradeStatus.Failed;
                    }
                    if (existedGrade.PracticalGrade < 5)
                    {
                        existedGrade.PracticalStatus = GradeStatus.Failed;
                    }
                    _context.Update(existedGrade);
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult GetStudenGrades([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int[] studentIds)
        {
            if (CheckToken(Authorization) == true)
            {
                var studentId = studentIds.Last();
                var grade = _context.Grade.Find(studentId, studentIds[0]);
                if (grade != null)
                {
                    return new JsonResult(grade);
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult CreateSubject([FromBody]Subject subject, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var existSubject = _context.Subject.Any(s => s.Name == subject.Name);
                if (existSubject == false)
                {
                    if (subject.FacultiesIds != null)
                    {
                        foreach (var facultyId in subject.FacultiesIds)
                        {
                            var faculty = _context.Faculty.Find(facultyId);
                            FacultySubject facultySubject = new FacultySubject
                            {
                                Subject = subject,
                                Faculty = faculty
                            };
                            _context.Add(facultySubject);
                        }
                    }
                    if (subject.CoursesIds != null)
                    {
                        foreach (var courseId in subject.CoursesIds)
                        {
                            var course = _context.Course.Find(courseId);
                            CourseSubject courseSubject = new CourseSubject
                            {
                                Subject = subject,
                                Course = course
                            };
                            _context.Add(courseSubject);
                        }
                    }
                    _context.Add(subject);
                    _context.SaveChanges();
                    return Ok();
                }
                return Conflict();
            }
            return Unauthorized();
        }

        public IActionResult DeleteSubject([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int subjectId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var subject = _context.Subject.Find(subjectId);
                if (subject != null)
                {
                    _context.Subject.Remove(subject);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult SubjectsIndex([FromHeader] string Authorization, [FromHeader] string Role)
        {
                var subjects = _context.Subject.ToList();
                return new JsonResult(subjects);
        }

        public IActionResult CreateDepartment([FromBody]Department department, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var existDepartment = _context.Department.Any(d => d.Name == department.Name);
                if (existDepartment == false)
                {
                    if (department.FacultiesIds != null)
                    {
                        foreach (var facultyId in department.FacultiesIds)
                        {
                            var faculty = _context.Faculty.Find(facultyId);
                            faculty.Department = department;
                            faculty.InDepartmentStatus = InDepartment.Yes;
                        }
                    }
                    _context.Add(department);
                    _context.SaveChanges();
                    return Ok();
                }
                return Conflict();
            }
            return Unauthorized();
        }

        public IActionResult CreateFaculty([FromBody]Faculty faculty, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var existFaculty= _context.Faculty.Any(f => f.Name == faculty.Name);
                if (existFaculty == false)
                {
                    var department = _context.Department.Find(faculty.DepartmentId);
                    faculty.Department = department;
                    faculty.InDepartmentStatus = InDepartment.Yes;
                    _context.Add(faculty);
                    _context.SaveChanges();
                    return Ok();
                }
                return Conflict();
            }
            return Unauthorized();
        }

        public IActionResult DepartmentsIndex([FromHeader] string Authorization, [FromHeader] string Role)
        {

                var departments = _context.Department
                    .Include(d => d.Faculties)
                    .ToList();
                return new JsonResult(departments);
        }

        public IActionResult FacultiesIndex([FromHeader] string Authorization, [FromHeader] string Role)
        {
                var faculties = _context.Faculty.ToList();
                return new JsonResult(faculties);
        }

        public IActionResult DepartmentDetails([FromBody] int departmentId)
        {
                var department = _context.Department
                     .Where(d => d.DepartmentId == departmentId)
                     .Include(d => d.Faculties)
                     .ToList();
                return new JsonResult(department);
        }

        public IActionResult EditDepartment([FromBody] Department department, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldDepartment = _context.Department
                    .Include(d => d.Faculties)
                    .SingleOrDefault(d => d.DepartmentId == department.DepartmentId);
                if(oldDepartment != null)
                {
                    oldDepartment.Name = department.Name;
                    oldDepartment.Description = department.Description;
                    oldDepartment.DescriptionDetails = department.DescriptionDetails;
                    oldDepartment.ImageUrl = department.ImageUrl;
                    _context.Update(oldDepartment);
                    _context.SaveChanges();
                    return new JsonResult (oldDepartment);
                }
                return NotFound();
            }
            return Unauthorized();
        }

        public IActionResult AddFaculty([FromBody] Department department, [FromHeader] string Authorization , [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldDepartment = _context.Department
                    .Include(d => d.Faculties)
                    .SingleOrDefault(d => d.DepartmentId == department.DepartmentId);
                if (oldDepartment != null)
                {
                    foreach(var facultyId in department.FacultiesIds)
                    {
                        var faculty = _context.Faculty.Find(facultyId);
                        faculty.Department = oldDepartment;
                        faculty.InDepartmentStatus = InDepartment.Yes;
                        _context.Update(faculty);
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound();
            }
            return Unauthorized();
        }

        public IActionResult AddClazz([FromBody] Faculty faculty, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldFaculty = _context.Faculty
                    .Include(f => f.Clazzs)
                    .SingleOrDefault(f => f.FacultyId == faculty.FacultyId);
                if (oldFaculty != null)
                {
                    foreach (var clazzId in faculty.ClazzIds)
                    {
                        var clazz = _context.Clazz.Find(clazzId);
                        clazz.Faculty = oldFaculty;
                        clazz.InFacultyStatus = InFaculty.Yes;
                        _context.Update(clazz);
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound();
            }
            return Unauthorized();
        }

        public IActionResult AddClazzToCourse([FromBody] Course course, [FromHeader] string Authorization , [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldCourse = _context.Course
                    .Include(f => f.Clazz)
                    .SingleOrDefault(f => f.CourseId == course.CourseId);
                if (oldCourse != null)
                {
                    foreach (var clazzId in course.ClazzIds)
                    {
                        var clazz = _context.Clazz.Find(clazzId);
                        clazz.Course = oldCourse;
                        clazz.InCourseStatus = InCourse.Yes;
                        _context.Update(clazz);
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound();
            }
            return Unauthorized();
        }

        public IActionResult FacultyDetails([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int facultyId)
        {
                var faculty = _context.Faculty
                     .Where(f => f.FacultyId == facultyId)
                     .Include(f => f.Department)
                     .Include(f => f.Clazzs)
                     .ToList();
                var facultySubjects = _context.FacultySubject
                    .Where(fs => fs.FacultyId == facultyId)
                    .Include(fs => fs.Subject);
                var result = new { facultySubjects, faculty};
                return new JsonResult(result);
        }

        public IActionResult EditFaculty([FromBody] Faculty faculty, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldFaculty = _context.Faculty.Find(faculty.FacultyId);
                oldFaculty.Name = faculty.Name;
                oldFaculty.Description = faculty.Description;
                oldFaculty.DescriptionDetails = faculty.DescriptionDetails;
                oldFaculty.ImageUrl = faculty.ImageUrl;
                oldFaculty.Department = faculty.Department;
                oldFaculty.Status = faculty.Status;
                _context.Update(oldFaculty);
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult DeleteStudentFromClazz([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] StudentClazz studentClazz)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var studentClazzs = _context.StudentClazz.Find(studentClazz.AccountId, studentClazz.ClazzId);
                if (studentClazzs != null)
                {
                    _context.Remove(studentClazzs);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteFacultyFromDepartment([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int[] faculty_departmentId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var department = _context.Department
                    .Include(d => d.Faculties)
                    .SingleOrDefault(d => d.DepartmentId == faculty_departmentId.Last());
                var faculty = _context.Faculty.Find(faculty_departmentId[0]);
                department.Faculties.Remove(faculty);
                faculty.InDepartmentStatus = InDepartment.No;
                    _context.Update(department);
                    _context.Update(faculty);
                    _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult DeleteClassFromFaculty([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int[] class_facultyId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var faculty = _context.Faculty
                    .Include(f => f.Clazzs)
                    .SingleOrDefault(f => f.FacultyId == class_facultyId.Last());
                var clazz = _context.Clazz.Find(class_facultyId[0]);
                faculty.Clazzs.Remove(clazz);
                clazz.InFacultyStatus = InFaculty.No;
                _context.Update(faculty);
                _context.Update(clazz);
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult DeleteClassFromCourse([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int[] class_courseId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var course = _context.Course
                    .Include(f => f.Clazz)
                    .SingleOrDefault(f => f.CourseId == class_courseId.Last());
                var clazz = _context.Clazz.Find(class_courseId[0]);
                course.Clazz.Remove(clazz);
                clazz.InCourseStatus = InCourse.No;
                _context.Update(course);
                _context.Update(clazz);
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult DeleteSubjectFromFaculty([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] FacultySubject facultySubject)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var facultySubjects = _context.FacultySubject.Find(facultySubject.FacultyId , facultySubject.SubjectId);
                if(facultySubjects != null)
                {
                    _context.Remove(facultySubjects);
                    _context.SaveChanges();
                    return new JsonResult(facultySubjects);
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteSubjectFromCourse([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] CourseSubject courseSubject)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var courseSubjects = _context.CourseSubject.Find(courseSubject.CourseId, courseSubject.SubjectId);
                if (courseSubjects != null)
                {
                    _context.Remove(courseSubjects);
                    _context.SaveChanges();
                    return new JsonResult(courseSubjects);
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult AddSubjectToFaculty([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] Faculty faculty)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var oldFaculty = _context.Faculty.Find(faculty.FacultyId);
                foreach (var subjectId in faculty.SubjectIds)
                {
                    var existedFacultySubject = _context.FacultySubject.Find(faculty.FacultyId , subjectId);
                    if (existedFacultySubject != null)
                    {
                        continue;
                    }
                    var subject = _context.Subject.Find(subjectId);
                    FacultySubject facultySubject = new FacultySubject
                    {
                        Faculty = oldFaculty,
                        Subject = subject
                    };
                    _context.Add(facultySubject);
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult AddSubjectToCourse([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] Course course)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var oldCourse = _context.Course.Find(course.CourseId);
                foreach (var subjectId in course.SubjectIds)
                {
                    var existedCourseSubject = _context.CourseSubject.Find(course.CourseId, subjectId);
                    if (existedCourseSubject != null)
                    {
                        continue;
                    }
                    var subject = _context.Subject.Find(subjectId);
                    CourseSubject courseSubject = new CourseSubject
                    {
                        Course = oldCourse,
                        Subject = subject
                    };
                    _context.Add(courseSubject);
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult GetStudenClazzs([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int studentId)
        {
            if (CheckToken(Authorization) == true)
            {
                var studentClazzs = _context.StudentClazz
                    .Where(sc => sc.AccountId == studentId)
                    .Include(sc => sc.Clazz)
                    .ToList();
                return new JsonResult(studentClazzs);
            }
            return Unauthorized();
        }

        public IActionResult ApplicationApproval([FromBody] int[] studentIds, [FromHeader] string Role, [FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var approvalStatus = studentIds.Last();
                var account = _context.Account
                    .Include(a => a.GeneralInformation)
                    .SingleOrDefault(a => a.AccountId == studentIds[0]);
                var builder = new StringBuilder();

                using (var reader = System.IO.File.OpenText(env.WebRootPath + "/Templates/Email Templates/ApprovalStatusEmail.html"))
                {
                    builder.Append(reader.ReadToEnd());
                }
                builder.Replace("{2}", account.GeneralInformation.Name);
                if (account == null)
                {
                    return NoContent();
                }
                if (approvalStatus == 0)
                {
                    account.ApprovalStatus = ApprovalStatus.Declined;
                    builder.Replace("{7}", "Your application has been declined!");
                    builder.Replace("{6}", "We are very sorry to announce that your application has been declined. For more info about this declination, please come to our nearest branch and meet our support staff");
                    builder.Replace("{8}", "none");
                }
                else if (approvalStatus == 1)
                {
                    account.ApprovalStatus = ApprovalStatus.Pending;
                    builder.Replace("{7}", "Your application is being reviewed!");
                    builder.Replace("{6}", "Thank you for choosing our university. Please wait a few days for us to verify your application. You will receive an email whether your application is accepted or declined. You can also use your temporary rollnumber below to check your approval status.");
                    builder.Replace("{8}", "none");
                }
                else if (approvalStatus == 2)
                {
                    account.ApprovalStatus = ApprovalStatus.Accepted;
                    builder.Replace("{7}", "Congratulations! Your application has been accepted.");
                    builder.Replace("{6}", "Welcome to the 2018-2019 academic year! We are counting on you to make this your best year yet at FPT APTECH. We are here to support you on every step of the way. Below is your information to login to your account.");
                    builder.Replace("{3}", "Email: " + account.GeneralInformation.Email);
                    builder.Replace("{4}", "Roll Number: " + account.RollNumber);
                    builder.Replace("{5}", "Password: " + account.GeneralInformation.Dob.ToString("ddMMyy"));
                }
                _context.Update(account);
                _context.SaveChanges();
                SendEmail("Welcome", builder.ToString(), account.GeneralInformation.Email, account.GeneralInformation.Name);
                return Ok();
            }
            return Unauthorized();
        }

        public async static Task SendEmail(string subject, string body, string receiverEmail, string receiverName)
        {
            var fromAddress = new MailAddress("from@gmail.com", "From FPT APTECH");
            var toAddress = new MailAddress(receiverEmail, "To " + receiverName);
            const string fromPassword = "fromPassword";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("projectaptechsem3@gmail.com", "Projectsem3")
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }
        }

        public IActionResult CreateCourse([FromBody]Course course, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var existCourse = _context.Course.Any(c => c.Name == course.Name);
                if (existCourse == false)
                {
                    _context.Add(course);
                    _context.SaveChanges();
                    return Ok();
                }
                return Conflict();
            }
            return Unauthorized();
        }

        public IActionResult CoursesIndex([FromHeader] string Authorization, [FromHeader] string Role)
        {

                var courses = _context.Course.ToList();
                return new JsonResult(courses);
        }

        public IActionResult CourseDetails([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int courseId)
        {

                var course = _context.Course
                     .Where(c => c.CourseId == courseId)
                     .Include(c => c.Clazz)
                     .ToList();
                var courseSubjects = _context.CourseSubject
                    .Where(c => c.CourseId == courseId)
                    .Include(fs => fs.Subject);
                var result = new { courseSubjects, course };
                return new JsonResult(result);
        }

        public IActionResult EditCourse([FromBody] Course course, [FromHeader] string Authorization , [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldCourse = _context.Course.Find(course.CourseId);
                oldCourse.Name = course.Name;
                oldCourse.Description = course.Description;
                oldCourse.DescriptionDetails = course.DescriptionDetails;
                oldCourse.Price = course.Price;
                oldCourse.ImageUrl = course.ImageUrl;
                oldCourse.Status = course.Status;
                _context.Update(oldCourse);
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult CreatePost([FromBody]Post post, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {

                    _context.Add(post);
                    _context.SaveChanges();
                    return Ok();
            }
            return Unauthorized();
        }

        public IActionResult PostIndex()
        {

                var post = _context.Post.ToList();
                return new JsonResult(post);
        }

        public IActionResult EditPost([FromBody] Post post, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var oldPost = _context.Post.Find(post.PostId);
                oldPost.Title = post.Title;
                oldPost.Description = post.Description;
                oldPost.ImageUrl = post.ImageUrl;
                oldPost.Content = post.Content;
                oldPost.Status = post.Status;
                _context.Update(oldPost);
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult PostDetails([FromBody] int postId)
        {
            var post = _context.Post.Find(postId);
            return new JsonResult(post);
        }

        public IActionResult DeletePost([FromBody] int postId, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var post = _context.Post.Find(postId);
                if (post != null)
                {
                    _context.Remove(post);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteClazz([FromHeader] string Authorization, [FromHeader] string Role, [FromBody] int clazzId)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role) == true)
            {
                var clazz = _context.Clazz.Find(clazzId);
                var studentClazzs = _context.StudentClazz.Where(sc => sc.ClazzId == clazzId);
                if (clazz != null)
                {
                    var faculty = _context.Faculty.Include(f => f.Clazzs).SingleOrDefault(f => f.FacultyId == clazz.FacultyId);
                    var course = _context.Course.Include(c => c.Clazz).SingleOrDefault(c => c.CourseId == clazz.CourseId);
                    if (faculty != null)
                    {
                        faculty.Clazzs.Remove(clazz);
                    }
                    if (course != null)
                    {
                        course.Clazz.Remove(clazz);
                    }
                    if (studentClazzs != null)
                    {
                        foreach (var studentClazz in studentClazzs)
                        {
                            _context.StudentClazz.Remove(studentClazz);
                        }
                    }
                    _context.Clazz.Remove(clazz);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteDepartment([FromBody] int departmentId, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var department = _context.Department.Include(d => d.Faculties).SingleOrDefault(d => d.DepartmentId == departmentId);
                if (department != null)
                {
                    department.Faculties.Clear();
                    _context.Remove(department);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteCourse([FromBody] int courseId, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var course = _context.Course.Include(c => c.Clazz).SingleOrDefault(c => c.CourseId == courseId);
                if (course != null)
                {
                    course.Clazz.Clear();
                    var courseSubjects = _context.CourseSubject.Where(cs => cs.CourseId == courseId);
                    _context.CourseSubject.RemoveRange(courseSubjects);
                    _context.Remove(course);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult DeleteFaculty([FromBody] int facultyId, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                var faculty = _context.Faculty.Include(f => f.Clazzs).SingleOrDefault(f => f.FacultyId == facultyId);
                if (faculty != null)
                {
                    faculty.Clazzs.Clear();
                    var department = _context.Department.Include(d => d.Faculties).SingleOrDefault(d => d.DepartmentId == faculty.FacultyId);
                    if (department != null)
                    {
                        department.Faculties.Remove(faculty);
                    }
                    var facultySubjects = _context.FacultySubject.Where(fs => fs.FacultyId == facultyId);
                    _context.FacultySubject.RemoveRange(facultySubjects);
                    _context.Remove(faculty);
                    _context.SaveChanges();
                    return Ok();
                }
                return NoContent();
            }
            return Unauthorized();
        }

        public IActionResult SendMultipleEmail([FromBody] Email email, [FromHeader] string Authorization, [FromHeader] string Role)
        {
            if (CheckToken(Authorization) == true && CheckPermission(Role))
            {
                foreach (var studentId in email.StudentIds)
                {
                    var account = _context.Account
                        .Include(a => a.GeneralInformation)
                        .SingleOrDefault(a => a.AccountId == studentId);
                    if (account == null)
                    {
                        continue;
                    }
                    var receiverEmail = account.GeneralInformation.Email;
                    var receiverName = account.GeneralInformation.Name;
                    SendEmail(email.Subject, email.Content, receiverEmail, receiverName);
                }
                return Ok();
            }
            return Unauthorized();    
        }

        public IActionResult StudentChooseCourse([FromBody] int[] student_courseId, [FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                var existCourseStudent = _context.CourseStudents.Find(student_courseId.Last(), student_courseId[0]);
                if(existCourseStudent != null)
                {
                    if(existCourseStudent.GraduatedDate > DateTime.Today)
                    {
                        return Conflict();
                    }
                }
                else
                {
                    var account = _context.Account.Find(student_courseId[0]);
                    var course = _context.Course.Find(student_courseId.Last());
                    if (account != null && course != null)
                    {
                        CourseStudent courseStudent = new CourseStudent
                        {
                            Course = course,
                            Account = account,
                        };
                        _context.CourseStudents.Add(courseStudent);
                        _context.SaveChanges();
                        return Ok();
                    }
                    return NoContent();
                }

            }
            return Unauthorized();
        }

        public IActionResult CourseSubjectExamDate([FromBody] Course course, [FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                foreach(var subjectId in course.SubjectIds)
                {
                    var courseSubject = _context.CourseSubject.Find(course.CourseId, subjectId);
                    if(courseSubject != null)
                    {
                        courseSubject.ExamDate = course.ExamDate;
                        _context.Update(courseSubject);
                    }
                    else
                    {
                        continue;
                    }
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult FacultySubjectExamDate([FromBody] Faculty faculty, [FromHeader] string Authorization)
        {
            if (CheckToken(Authorization) == true)
            {
                foreach (var subjectId in faculty.SubjectIds)
                {
                    var facultySubject = _context.FacultySubject.Find(faculty.FacultyId, subjectId);
                    if (facultySubject != null)
                    {
                        facultySubject.ExamDate = faculty.ExamDate;
                        _context.Update(facultySubject);
                    }
                    else
                    {
                        continue;
                    }
                }
                _context.SaveChanges();
                return Ok();
            }
            return Unauthorized();
        }

        public IActionResult GetExamDate([FromBody] int studentId, [FromHeader] string Authorization)
        {
                var studentClazzs = _context.StudentClazz
                    .Where(sc => sc.AccountId == studentId)
                    .Include(sc => sc.Clazz)
                    .ThenInclude(c => c.Faculty)
                    .ThenInclude(f => f.FacultySubjects)
                    .Include(sc => sc.Clazz)
                    .ThenInclude(c => c.Course)
                    .ThenInclude(c => c.CourseSubjects);
                var clazzIds = new List<int>();
                foreach (var studentClazz in studentClazzs)
                {
                    var clazzId = studentClazz.ClazzId;
                    clazzIds.Add(clazzId);
                }

                return new JsonResult(clazzIds);
        }

        public bool CheckToken(string accessToken)
        {
            var credential = _context.Credential.SingleOrDefault(t => t.AccessToken == accessToken);
            if (credential != null && credential.IsValid())
            {
                return true;
            }
            return false;
        }


        public bool CheckPermission(string role)
        {
            if (role.Split("#").Contains("Employee"))
            {
                return true;
            }
            return false;
        }
    }
}
