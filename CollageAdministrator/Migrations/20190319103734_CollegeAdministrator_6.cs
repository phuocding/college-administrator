﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CollageAdministrator.Migrations
{
    public partial class CollegeAdministrator_6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Course",
                table: "GeneralInformation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Course",
                table: "GeneralInformation");
        }
    }
}
