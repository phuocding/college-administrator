﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CollageAdministrator.Migrations
{
    public partial class CollegeAdministrator_9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Course",
                table: "GeneralInformation");

            migrationBuilder.AddColumn<DateTime>(
                name: "GraduatedDate",
                table: "CourseStudents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "JoinedDate",
                table: "CourseStudents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GraduatedDate",
                table: "CourseStudents");

            migrationBuilder.DropColumn(
                name: "JoinedDate",
                table: "CourseStudents");

            migrationBuilder.AddColumn<string>(
                name: "Course",
                table: "GeneralInformation",
                nullable: true);
        }
    }
}
