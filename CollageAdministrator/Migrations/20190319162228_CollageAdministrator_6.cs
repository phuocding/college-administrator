﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CollageAdministrator.Migrations
{
    public partial class CollageAdministrator_6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionDetails",
                table: "Faculty",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionDetails",
                table: "Department",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionDetails",
                table: "Course",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescriptionDetails",
                table: "Faculty");

            migrationBuilder.DropColumn(
                name: "DescriptionDetails",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DescriptionDetails",
                table: "Course");
        }
    }
}
