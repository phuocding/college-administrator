﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class GeneralInformation
    {
        [Key]
        public int AccountId { get; set; }
        public string Name { get; set; }
        public UserGender Gender { get; set; }
        [Display(Name = "Birthday")]
        public DateTime Dob { get; set; }
        public string Phone { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string ResidentialAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Stream { get; set; }
        public string Field { get; set; }
        public string PreviousUniversity { get; set; }
        public string Center { get; set; }
        public string EnrollmentNumber { get; set; }
        public string PreviousStream { get; set; }
        public string PreviousField { get; set; }
        public string Mark { get; set; }
        public string Outof { get; set; }
        public string ClazzObtained { get; set; }
        public string SportDetails { get; set; }
        public Account Account { get; set; }
        public enum UserGender
        {
            Female = 0,
            Male = 1,
            Other = 2
        }
    }
}
