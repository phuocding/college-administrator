﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Clazz
    {
        public Clazz()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = ClazzStatus.Active;
            this.InFacultyStatus = InFaculty.No;
            this.InCourseStatus = InCourse.No;
        }
        [Key]
        public int ClazzId { get; set; }
        public string Name { get; set; }
        public string Teacher { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public ClazzStatus Status { get; set; }
        public Faculty Faculty { get; set; }
        public Course Course { get; set; }
        public List<StudentClazz> StudentClazzs { get; set; }
        [NotMapped]
        public int CourseId { get; set; }
        [NotMapped]
        public int FacultyId { get; set; }
        public InCourse InCourseStatus { get; set; }
        public InFaculty InFacultyStatus { get; set; }
    }

    public enum ClazzStatus
    {
        Active = 1,
        Deactive = 0
    }

    public enum InFaculty
    {
        Yes = 1,
        No = 0
    }

    public enum InCourse
    {
        Yes = 1,
        No = 0
    }
}
