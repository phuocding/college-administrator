﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class CourseStudent
    {
        public CourseStudent()
        {
            this.JoinedDate = DateTime.Today;
            this.GraduatedDate = DateTime.Today.AddDays(90);
        }
        public int CourseId { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
        public Course Course { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime GraduatedDate { get; set; }
    }
}
