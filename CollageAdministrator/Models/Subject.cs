﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Subject
    {
        public Subject()
        {
            this.Status = SubjectStatus.Active;
        }
        [Key]
        public int SubjectId { get; set; }
        public string Name { get; set; }
        public SubjectStatus Status { get; set; }
        public List<FacultySubject> FacultySubjects { get; set; }
        public List<CourseSubject> CourseSubjects { get; set; }
        [NotMapped]
        public int[] FacultiesIds { get; set; }
        [NotMapped]
        public int[] CoursesIds { get; set; }
    }

    public enum SubjectStatus
    {
        Active = 1,
        Deactive = 0
    }
}
