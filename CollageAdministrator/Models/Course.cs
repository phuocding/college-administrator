﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Course
    {
        public Course()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = CourseStatus.Active;
        }
        [Key]
        public int CourseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionDetails { get; set; }
        public string ImageUrl { get; set; }
        public float Price { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public CourseStatus Status { get; set; }
        public List<Clazz> Clazz { get; set; }
        public List<CourseSubject> CourseSubjects { get; set; }
        public List<CourseStudent> CourseStudent { get; set; }
        [NotMapped]
        public int[] SubjectIds { get; set; }
        [NotMapped]
        public int[] ClazzIds { get; set; }
        [NotMapped]
        public DateTime ExamDate { get; set; }
    }

    public enum CourseStatus
    {
        Active = 1,
        Deactive = 0
    }
}
