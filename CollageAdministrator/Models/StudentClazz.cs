﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class StudentClazz
    {
        public StudentClazz()
        {
            this.Status = StudentClazzStatus.Active;
            this.JoinedDate = DateTime.Today;
            this.GraduatedDate = DateTime.Today.AddDays(90);
        }
        public int AccountId { get; set; }
        public int ClazzId { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime GraduatedDate { get; set; }
        public Account Account { get; set; }
        public Clazz Clazz { get; set; }
        public StudentClazzStatus Status { get; set; }

    }

    public enum StudentClazzStatus
    {
        Active = 1,
        Deactive = 0
    }
}
