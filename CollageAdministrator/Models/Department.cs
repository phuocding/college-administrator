﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Department
    {
        public Department()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = DepartmentStatus.Active;
        }
        [Key]
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string DescriptionDetails { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DepartmentStatus Status { get; set; }
        public List<Faculty> Faculties { get; set; }
        [NotMapped]
        public int[] FacultiesIds { get; set; }
    }

    public enum DepartmentStatus
    {
        Active = 1,
        Deactive = 0
    }
}
