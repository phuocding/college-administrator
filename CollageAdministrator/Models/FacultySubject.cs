﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class FacultySubject
    {
        public int SubjectId { get; set; }
        public int FacultyId { get; set; }
        public Subject Subject { get; set; }
        public Faculty Faculty { get; set; }
        public DateTime ExamDate { get; set; }
    }
}
