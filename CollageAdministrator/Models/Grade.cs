﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Grade
    {

        public Grade()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.AssignmentStatus = GradeStatus.Passed;
            this.TheoreticalStatus = GradeStatus.Passed;
            this.PracticalStatus = GradeStatus.Passed;
        }
        public int AccountId { get; set; }
        public int SubjectId { get; set; }
        public float AssignmentGrade { get; set; }
        public float PracticalGrade { get; set; }
        public float TheoreticalGrade { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public GradeStatus TheoreticalStatus { get; set; }
        public GradeStatus PracticalStatus { get; set; }
        public GradeStatus AssignmentStatus { get; set; }
        public Account Account { get; set; }
        public Subject Subject { get; set; }
    }

    public enum GradeStatus
    {
        Failed = 0,
        Passed = 1,
    }
}
