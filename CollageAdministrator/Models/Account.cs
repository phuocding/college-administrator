﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Account
    {
        public Account()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = AccountStatus.Active;
            this.Salt = new byte[128 / 8];
            this.Password = "a";
            this.RollNumber = "a";
            this.ApprovalStatus = ApprovalStatus.Pending;
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(this.Salt);
            }
        }

        public void EncryptPassword(string password)
        {
            this.Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: this.Salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));
        }
        [Key]
        public int AccountId { get; set; }
        public string RollNumber { get; set; }
        public string Password { get; set; }
        public byte[] Salt { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public AccountStatus Status { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public GeneralInformation GeneralInformation { get; set; }
        public List<StudentClazz> StudentClazzs { get; set; }
        public List<Grade> Grades { get; set; }
        public List<RoleAccount> RoleAccounts { get; set; }
        public List<CourseStudent> CourseStudent { get; set; }

    }

    public enum AccountStatus
    {
        Active = 1,
        Deactive = 0
    }

    public enum ApprovalStatus
    {
        Declined = 0,
        Pending = 1,
        Accepted = 2
    }
}
