﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Email
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public int[] StudentIds { get; set; }
    }
}
