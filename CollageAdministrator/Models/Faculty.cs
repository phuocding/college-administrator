﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Faculty
    {
        public Faculty()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = FacultyStatus.Active;
            this.InDepartmentStatus = InDepartment.No;
        }
        [Key]
        public int FacultyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string DescriptionDetails { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public FacultyStatus Status { get; set; }
        public Department Department { get; set; }
        public List<Clazz> Clazzs { get; set; }
        public List<FacultySubject> FacultySubjects { get; set; }
        [NotMapped]
        public int DepartmentId { get; set; }
        [NotMapped]
        public int[] SubjectIds { get; set; }
        [NotMapped]
        public int[] ClazzIds { get; set; }
        public InDepartment InDepartmentStatus { get; set; }
        [NotMapped]
        public DateTime ExamDate { get; set; }
    }

    public enum FacultyStatus
    {
        Active = 1,
        Deactive = 0
    }

    public enum InDepartment
    {
        Yes = 1,
        No = 0
    }
}
