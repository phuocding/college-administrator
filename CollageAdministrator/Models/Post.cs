﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollageAdministrator.Models
{
    public class Post
    {
        public Post()
        {
            this.CreatedAt = DateTime.Today;
            this.UpdatedAt = DateTime.Today;
            this.Status = PostStatus.Active;
        }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public PostStatus Status { get; set; }
    }

    public enum PostStatus
    {
        Active = 1,
        Deactive = 0
    }
}
